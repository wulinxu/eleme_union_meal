/**
 * Created by jia on 2015/10/9.
 */
var server_host = "http://127.0.0.1:8888"
var changeMenuName = (function() {
    var menu_selector_content = ".menu-cart.menu-cart-box-shadow";
    var plus_div_content = "<div class='menu-cart menu-cart-box-shadow plus_div plus-div'></div>";
    var plus_div_button_content = "<a class='cart-desc plus-submit fr'>买买买</a>";

    var menu = $(menu_selector_content)
    //menu.css("height", "78px")

    menu.parent().append(plus_div_content)
    var plus_div = $(".plus_div")
    plus_div.append(plus_div_button_content)
    var name_text = document.createElement("input")
    name_text.type = "text"
    name_text.className = "input-name embed-method-name"
    name_text.placeholder="请输入你的名字."

    if(typeof(Storage)!=="undefined") {
        if (localStorage.embed_username){
            name_text.value=localStorage.embed_username
        }
    }
    plus_div.append(name_text)

    var menu_category = menu.find(".basket_list")
    var submit_category = menu.find(".basket_wrap")
    var submit = submit_category.find('.rcart-checkout.c_c')

    if (submit.length != 0){
        submit.removeClass('c_c')
        submit.addClass('disabled')
        submit.css('disable', 'none')
    }
    //名字
    var title_category = menu.find(".rcart-title")
    if (title_category.find('.embed-method-name').length == 0){
        //var name_text = document.createElement("input")
        //name_text.type = "text"
        //name_text.className = "embed-method-name"
        //name_text.placeholder="请输入你的名字."
        //if(typeof(Storage)!=="undefined") {
        //    if (localStorage.embed_username){
        //        name_text.value=localStorage.embed_username
        //    }
        //}

        //title_category.append(name_text)
    }
});

$(document).on("click", ".plus-submit", function(e) {

    var menu = $(".menu-cart")
    var name = $(".embed-method-name").val()
    if (!name){
        alert('名字不能为空')
        return 0;
    }
    var menu_list = []
    menu.find('.item-list').find('.normal-item').each(function() {
        menu_list.push({
            'name': $(this).find('.item-name').text(),
            'amount': $(this).find('.item-count input.item-count').val(),
            'unit': $(this).find('.item-price').text().slice(1)
        })
    })
    var server_addr = server_host + "/api/py/add_menu?name="
        + encodeURIComponent(name)
        + "&menu="
        + encodeURIComponent(JSON.stringify(menu_list))
        + "&shop_url="
        + encodeURIComponent(window.location.origin + window.location.pathname)
    $(this).attr("href", server_addr)

});

$(document).on("change", ".embed-method-name", function(e) {
    if(typeof(Storage)!=="undefined") {
        localStorage.embed_username = $(this).val()
    }
})



changeMenuName();





